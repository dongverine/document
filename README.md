# 문서 리스트 요약
* JAVA
    - [VisualVM](https://gitlab.com/dongverine/document/-/blob/master/doc/JAVA_VisualVM.md)

* SpringBoot
    - [application.properties 설정](https://gitlab.com/dongverine/document/-/blob/master/doc/SpringBoot.Properties/index.md)

* Logback
    - [Logback 설정](https://gitlab.com/dongverine/document/-/blob/master/doc/Logback/index.md)
